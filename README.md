build.gradle
1. Add this to your project gradle file
```groovy
allprojects {
    repositories {
        ...
        ...
        maven {
            url 'https://gitlab.com/api/v4/projects/52152133/packages/maven'
        }
    }
}
```

2. Add dependency to your app gradle file
```groovy
dependencies {
    implementation 'com.nexstream.NexIDSDK:NexIDSDK:1.4.0'
}
```

3. minimum android sdk version : 26

4. *For release apk*, you will need to add this rule to your proguard file
```gradle
-keep public class com.nexstream.** {*;}
-keep public class com.megvii.** {*;}
```

5. Example
```java
import com.nexstream.NexIDSDK.*;

// please allow few seconds for this to complete the process
// suggest this to be performed at very beginning of the activity
NexIDSDK.init("your_apiKey", "your_appId");

// 0 - IC Reader, 1 - Passport Reader
// result will be return in onActivityResult
NexIDSDK.startDocumentScan(yourMainActivityClass, 0); 

// result will be return in onActivityResult
NexIDSDK.startBarcodeScan(yourMainActivityClass);

// supported livenessType - "still" , "meglive" , "flash"
NexIDSDK.startLivenessScan(yourMainActivity, "referenceImageInBase64", "still", new ActionListenerCallback() { 
    @Override
    public void onActionSuccess(Object o) {
        // return object can be a String or EkycResult()
        // some actions to be done when success callback ...
    }

    @Override
    public void onActionFailure(Exception e) {
        // some actions to be done when fail callback ...
    }

    @Override
    public void onActionCanceled() {
        // some actions to be done when cancel callback ...
    }
});
```

6. Reference

Environment:
Java 17
Android Gradle Plugin 7.3.0
Gradle version 7.5.1


If you faced this issue: 
```
Execution failed for task ':app:processStagingDebugMainManifest'.
> Manifest merger failed : Attribute application@allowBackup value=(true) from AndroidManifest.xml:6:9-35
  	is also present at [com.nexstream.NexIDSDK:NexIDSDK:1.0.0] AndroidManifest.xml:15:9-36 value=(false).
```
**Suggestion: add 'tools:replace="android:allowBackup"' to <application> element at AndroidManifest.xml to override.**

7. Possible Liveness Result (Status Code and Message)
- 1000 : Live verification passed, comparison passed. (Will deduct balance)
- 2000 : Live verification passed, comparison failed. (Will deduct balance)
- 3000 : Reference data error, possible reasons: no such ID number, photo format error, face not found in the photo, etc. (Will deduct balance)
- 400 : No face found on the reference image. (Will not deduct balance)
